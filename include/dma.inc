
	IF !DEF(DMA_INC)
	; don't re-include this file if it's already been INCLUDE'd
DEF DMA_INC EQU 1


DEF DMA_ROUTINE	EQU $FF80


MACRO dma_Copy2HRAM
; copies the dmacode to HIRAM. dmacode will get run each Vblank,
; and it is resposible for copying sprite data from ram to vram.
; dma_Copy2HRAM trashes all registers
; actual dma code preserves all registers
	jr	.copy_dma_into_memory\@
.dmacode\@
	push	af
	ld	a, OAMDATALOCBANK
	ldh	[rDMA], a
	ld	a, $40 ; countdown until DMA is finishes, then exit
.dma_wait\@			;<-|
	dec	a		;  |	keep looping until DMA finishes
	jr	nz, .dma_wait\@ ; _|
	pop	af
	ret	
.dmaend\@
.copy_dma_into_memory\@
	ld	de, DMA_ROUTINE
	ld	hl, .dmacode\@
	ld	bc, .dmaend\@ - .dmacode\@
	; copies BC # of bytes from source (HL) to destination (DE)
	call	mem_Copy
	ENDM






	ENDC	; end definition of DMA.inc file
