include "hardware.inc"

SECTION "Header", ROM0
        NINTENDO_LOGO
        DB "Play 2048!     "         ; Cart name - 15 characters / 15 bytes
        DB CART_COMPATIBLE_DMG       ; $143 - GBC support. $80 = both. $C0 = only gbc
        DB 0,0                       ; $144 - Licensee code (not important)
        DB 0                         ; $146 - SGB Support indicator
        DB CART_ROM                  ; $147 - Cart type / MBC type (0 => no mbc)
        DB 0                         ; $148 - ROM Size (0 => 32KB)
        DB 0                         ; $149 - RAM Size (0 => 0KB RAM on cartridge)
        DB 1                         ; $14a - Destination code
        DB $33                       ; $14b - Old licensee code
        DB 0                         ; $14c - Mask ROM version
        DB 0                         ; $14d - Complement check (important) rgbds-fixed
        DW 0                         ; $14e - Checksum (not important)