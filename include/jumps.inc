; A <> B
jne:    MACRO
        jp nz, \1
        ENDM

; A == B
jeq:    MACRO
        jp z, \1
        ENDM

; A < B
jl:     MACRO
        jp c, \1
        ENDM

; A > B
jg:     MACRO
        jr c, 3
        jp nz, \1
        ENDM

; A <= B
jle:    MACRO
        jp c, \1
        jp z, \1
        ENDM

; A >= B
jge:    MACRO
        jp nc, \1
        ENDM

; NOT A < B
jnl:    MACRO
        jge \1
        ENDM

; NOT A > B
jng:    MACRO
        jle \1
        ENDM