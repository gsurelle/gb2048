; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

include "hardware.inc"

SECTION "MainMenu", ROM0

DEF MENU_TEXT_TITLE_Y  EQU 4
DEF MENU_TEXT_ACTION_Y EQU 12

MainMenu_Init::
       call StopLCD 

        ; Load font in the VRAM.
        ld hl, CharsTable
        ld de, _VRAM
        ld bc, CharsTableEnd - CharsTable
        call mem_CopyMono

        call DisableSprites

        ; Restart the LCD.
        call StartLCD

        ; Clear the background with spaces.
        ld a, $20 
        ld hl, _SCRN0
        ld bc, SCRN_VX_B * SCRN_VY_B
        call mem_SetVRAM

        ; Draw the title.
        ld hl, MenuTextTitle
        ld de, _SCRN0 + (SCRN_VX_B * MENU_TEXT_TITLE_Y)
        ld bc, MenuTextTitleEnd - MenuTextTitle
        call mem_CopyVRAM

        ; Draw the action.
        ld hl, MenuTextAction
        ld de, _SCRN0 + (SCRN_VX_B * MENU_TEXT_ACTION_Y)
        ld bc, MenuTextActionEnd - MenuTextAction
        call mem_CopyVRAM

        ret

MainMenu_Process::
        ; Game start when the player press Start.
        ld a, [wIntputPressEvent]
        and a, PADF_START

        ret

MenuTextTitle:
        DB "     Play  2048                 "
                                ; <- Screen size
MenuTextTitleEnd:
MenuTextAction:
        DB "    Press  START                "
                                ; <- Screen size
MenuTextActionEnd:

