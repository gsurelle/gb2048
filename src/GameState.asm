; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

include "hardware.inc"
include "src/Grid.inc"


SECTION "Game", ROM0

TileSet:
INCBIN "tiles.2bpp"
TileSetEnd:

Game_Init::
        call StopLCD

        ld a, %11100100
        ld [rBGP], a

        ; Load tiles
        ld hl, TileSet
        ld de, _VRAM
        ld bc, TileSetEnd - TileSet
        call mem_Copy

        call DisableSprites

        ; Initialize Cells phases
        xor a
        ld hl, wCurrentCellPhasesTable
        ld bc, CELLS_COUNT
        call mem_Set

        call CreateNewPhase
        call CreateNewPhase
        ; Force start
        ;ld a, 2
        ;ld [wCurrentCellPhasesTable + CELL_X2_Y0], a
        ;ld a, 2
        ;ld [wCurrentCellPhasesTable + CELL_X3_Y0], a
        
        ;call DrawGrid
        ;ld a, 2
        ;ld [wCurrentCellPhasesTable], a
        ld a, $F2 
        ld hl, _SCRN0
        ld bc, SCRN_VX_B * SCRN_VY_B
        call mem_SetVRAM
        call DrawGrid

        call StartLCD

        xor a
        ld [wAnEventOccurred], a

        ret

Game_Process::
        call UpdateJoypadEvents
        ; Game start when the player press Start.
        ld a, [wIntputPressEvent]
        and PADF_RIGHT
        jr nz, .move_right

        ld a, [wIntputPressEvent]
        and PADF_LEFT
        jr nz, .move_left

        ld a, [wIntputPressEvent]
        and PADF_UP
        jr nz, .move_up

        ld a, [wIntputPressEvent]
        and PADF_DOWN
        jr nz, .move_down

        jr .end_moves

.move_right
        call PushRight
        jr .end_moves

.move_left
        call PushLeft
        jr .end_moves

.move_up
        call PushUp
        jr .end_moves

.move_down
        call PushDown

.end_moves
        ; A new phase is created when a move occured
        ; (a merge involve a least one move).
        ld a, [wAnEventOccurred]
        and a
        jr z, .skip
        
        call CreateNewPhase

        call WaitVBlank
        call DrawGrid
        jr .after_phase_creation

.skip
        call WaitVBlank

.after_phase_creation

        xor a
        ld [wAnEventOccurred], a
        ret


SECTION "Game_Variables", WRAM0


; cells phases. 
; $00 = Empty
; $01 = Phase 1
; $02 = Phase 2
; Etc..
wCurrentCellPhasesTable::           ds CELLS_COUNT