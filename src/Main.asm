; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

include "hardware.inc"
include "sprite.inc"
include "dma.inc"

include "vectors.inc"

section "Entry", rom0[$100]
entry_point:
        di ; disable interrupts.
        jp MainProgram

include "headers.inc"

; Game main program
SECTION "Main", ROM0[$150]

MainProgram:
        dma_Copy2HRAM

        di 
        ld sp, $ffff

	ld	a, IEF_VBLANK	; --
	ld	[rIE], a	; Set only Vblank interrupt flag
	ei			; enable interrupts. Only vblank will trigger

        ; Clear the sprite working buffer.
        ld a, $00 
        ld hl, OAMDATALOC
        ld bc, $100
        call mem_Set


        call InitJoypad
        call ClearJoypadEvents

MainMenu:
        call MainMenu_Init

.loop
        call WaitVBlank
        call UpdateJoypadEvents
        call MainMenu_Process

        ; while the routine return 0 in a.
        and a
        jp z, .loop

Game:
        call Game_Init

.loop
        call Game_Process

        ; while the routine return 0 in a.
        and a
        jp z, .loop
        jp MainMenu