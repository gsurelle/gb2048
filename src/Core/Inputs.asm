; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

include "hardware.inc"

SECTION "Input", ROM0

InitJoypad::
	ld a, $00
	ld [wIntputPressEvent], a
	ld [wInputLastState], a

GetJoypadKeys::
; Uses AF, B
; get currently pressed keys. Register A will hold keys in the following
; order: MSB --> LSB (Most Significant Bit --> Least Significant Bit)
; Down, Up, Left, Right, Start, Select, B, A
; This works by writing

	; get action buttons: A, B, Start / Select
	ld	a, P1F_GET_NONE
	xor	P1F_GET_BTN; choose bit that'll give us action button info
	ld	[rP1], a; write to joypad, telling it we'd like button info
	ld	a, [rP1]; gameboy will write (back in address) joypad info
	ld	a, [rP1]
	cpl		; take compliment
	and	$0f	; look at first 4 bits only  (lower nibble)
	swap	a	; place lower nibble into upper nibble
	ld	b, a	; store keys in b
	; get directional keys
	ld 	a, P1F_GET_NONE
	xor 	P1F_GET_DPAD
	ld	[rP1], a ; write to joypad, selecting direction keys
	ld	a, [rP1]
	ld	a, [rP1]
	ld	a, [rP1]	; delay to reliablly read keys
	ld	a, [rP1]	; since we've just swapped from reading
	ld	a, [rP1]	; buttons to arrow keys
	ld	a, [rP1]
	cpl			; take compliment
	and	$0f		; keep lower nibble
	or	b		; combine action & direction keys (result in a)
	ld	b, a

	ld	a, P1F_GET_BTN | P1F_GET_DPAD
	ld	[rP1], a		; reset joypad

	ld	a, b	; register A holds result. Each bit represents a key
	ret

UpdateJoypadEvents::
	call GetJoypadKeys
	
	; Keep of the keys pressed now.
	ld b, a
	ld hl, wInputLastState
	ld a, [hl]
	cpl 
	and b

	; Keep of the state for the next frame
	ld [hl], b

	; Globalize the frame inputs pressed
	ld [wIntputPressEvent], a

	ret

ClearJoypadEvents::
	ld a, $00
	ld [wIntputPressEvent], a
	ret

SECTION "Input_vars", WRAM0
wIntputPressEvent::	DS 1
wInputLastState:	DS 1
