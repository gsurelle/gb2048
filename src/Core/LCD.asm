; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

SECTION "lcd", ROM0

INCLUDE "hardware.inc"

StopLCD::
        ; Return if the LCD is alread off.
	ld	a, [rLCDC]	   
	and	LCDCF_ON	    
	ret	z		         

        ;LCD stop can't be done when it is drawing.
        call WaitVBlank    

        ; Toggle the LCD off.
	ld	a, [rLCDC]      
	xor	LCDCF_ON	    
	ld	[rLCDC], a	   
	ret

StartLCD::
        ld a, [rLCDC]
        or LCDCF_ON
        ld [rLCDC], a
        ret

WaitVBlank::           
	; We use the Vblank interrupt to wake up the CPU at the good moment.
	halt 
	nop 
	ret

EnableSprites::
	ld	a, [rLCDC]	; fetch LCD Config. (Each bit is a flag)
	or	LCDCF_OBJON	; enable sprites through "OBJects ON" flag
	or	LCDCF_OBJ16	; enable 8bit wide sprites (vs. 16-bit wide)
	ld	[rLCDC], a	; save LCD Config. Sprites are now visible. 
	ret

DisableSprites::
	ld	a, [rLCDC]	; fetch LCD Config. (Each bit is a flag)
	res	1, a
	ld	[rLCDC], a	; save LCD Config. Sprites are now visible. 
	ret
