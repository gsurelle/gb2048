; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

include "src/Grid.inc"

SECTION "GridMovements", ROM0

; +---+---+---+---+
; | 0 | 1 | 2 | 3 |
; +---+---+---+---+
; | 4 | 5 | 6 | 7 |
; +---+---+---+---+
; | 8 | 9 | a | b |
; +---+---+---+---+
; | c | d | e | f |
; +---+---+---+---+

; 0 = 3
; 1 = 2
; 2 = 1
; 3 = 0
MACRO MergeDownCol
        ; Merge 1 into 0
        ld d, CELL_X\1_Y2
        ld e, CELL_X\1_Y3
        call MergeTwoCells

        ; Merge 1 into 2, if 2 not merged into 3
        and a
        jr nz, .merge_col\1_0_into_1
        ld d, CELL_X\1_Y1
        ld e, CELL_X\1_Y2
        call MergeTwoCells

        ; Merge 0 into 1
.merge_col\1_0_into_1:
        ld d, CELL_X\1_Y0
        ld e, CELL_X\1_Y1
        call MergeTwoCells
        ENDM

MACRO MoveDownCol_one_pass_m
MoveDownCol_one_pass_X\1:
        ld d, CELL_X\1_Y2
        ld e, CELL_X\1_Y3
        call MoveCell
        ld d, CELL_X\1_Y1
        ld e, CELL_X\1_Y2
        call MoveCell
        ld d, CELL_X\1_Y0
        ld e, CELL_X\1_Y1
        call MoveCell

        ret
        ENDM
MACRO MoveDownCol
        MoveDownCol_one_pass_m \1
MoveDownCol_Y\1:
        ; 3 passes
        call MoveDownCol_one_pass_X\1
        call MoveDownCol_one_pass_X\1
        call MoveDownCol_one_pass_X\1
        ret
        ENDM

        MoveDownCol 0
        MoveDownCol 1
        MoveDownCol 2
        MoveDownCol 3

PushDown::
        xor a
        ld [wAnEventOccurred], a

        ; A first push
        call MoveDownCol_Y0
        call MoveDownCol_Y1
        call MoveDownCol_Y2
        call MoveDownCol_Y3

        ; The merge of cells
        MergeDownCol 0
        MergeDownCol 1
        MergeDownCol 2
        MergeDownCol 3

        ; A second push, because the merge create empty cells.
        call MoveDownCol_Y0
        call MoveDownCol_Y1
        call MoveDownCol_Y2
        call MoveDownCol_Y3

        ret

MACRO MergeUpCol
        ; Merge 1 into 0
        ld d, CELL_X\1_Y1
        ld e, CELL_X\1_Y0
        call MergeTwoCells

        ; Merge 2 into 1, if 1 not merged into 0
        and a
        jr nz, .merge_col\1_3_into_2
        ld d, CELL_X\1_Y2
        ld e, CELL_X\1_Y1
        call MergeTwoCells

        ; Merge 3 into 2
.merge_col\1_3_into_2:
        ld d, CELL_X\1_Y3
        ld e, CELL_X\1_Y2
        call MergeTwoCells
        ENDM

MACRO MoveUpCol_one_pass_m
MoveUpCol_one_pass_X\1:
        ld d, CELL_X\1_Y1
        ld e, CELL_X\1_Y0
        call MoveCell
        ld d, CELL_X\1_Y2
        ld e, CELL_X\1_Y1
        call MoveCell
        ld d, CELL_X\1_Y3
        ld e, CELL_X\1_Y2
        call MoveCell

        ret
        ENDM
MACRO MoveUpCol
        MoveUpCol_one_pass_m \1
MoveUpCol_Y\1:
        ; 3 passes
        call MoveUpCol_one_pass_X\1
        call MoveUpCol_one_pass_X\1
        call MoveUpCol_one_pass_X\1
        ret
        ENDM

        MoveUpCol 0
        MoveUpCol 1
        MoveUpCol 2
        MoveUpCol 3

PushUp::
        xor a
        ld [wAnEventOccurred], a

        ; A first push
        call MoveUpCol_Y0
        call MoveUpCol_Y1
        call MoveUpCol_Y2
        call MoveUpCol_Y3

        ; The merge of cells
        MergeUpCol 0
        MergeUpCol 1
        MergeUpCol 2
        MergeUpCol 3

        ; A second push, because the merge create empty cells.
        call MoveUpCol_Y0
        call MoveUpCol_Y1
        call MoveUpCol_Y2
        call MoveUpCol_Y3

        ret

MACRO MergeRightLine
        ; Line 1
        ; Merge 2 into 3
        ld d, CELL_X2_Y\1
        ld e, CELL_X3_Y\1
        call MergeTwoCells

        ; Merge 1 into 2, if 2 not merged into 3
        and a
        jr nz, .merge_line\1_0_into_1
        ld d, CELL_X1_Y\1
        ld e, CELL_X2_Y\1
        call MergeTwoCells

        ; Merge 0 into 1
.merge_line\1_0_into_1:
        ld d, CELL_X0_Y\1
        ld e, CELL_X1_Y\1
        call MergeTwoCells
        ENDM

MACRO MoveRightLine_one_pass_m
MoveRightLine_one_pass_Y\1:
        ld d, CELL_X2_Y\1
        ld e, CELL_X3_Y\1
        call MoveCell
        ld d, CELL_X1_Y\1
        ld e, CELL_X2_Y\1
        call MoveCell
        ld d, CELL_X0_Y\1
        ld e, CELL_X1_Y\1
        call MoveCell

        ret
        ENDM
MACRO MoveRightLine
        MoveRightLine_one_pass_m \1
MoveRightLine_Y\1:
        ; 3 passes
        call MoveRightLine_one_pass_Y\1
        call MoveRightLine_one_pass_Y\1
        call MoveRightLine_one_pass_Y\1
        ret
        ENDM

        MoveRightLine 0
        MoveRightLine 1
        MoveRightLine 2
        MoveRightLine 3

PushRight::
        xor a
        ld [wAnEventOccurred], a

        ; A first push
        call MoveRightLine_Y0
        call MoveRightLine_Y1
        call MoveRightLine_Y2
        call MoveRightLine_Y3

        ; The merge of cells
        MergeRightLine 0
        MergeRightLine 1
        MergeRightLine 2
        MergeRightLine 3

        ; A second push, because the merge create empty cells.
        call MoveRightLine_Y0
        call MoveRightLine_Y1
        call MoveRightLine_Y2
        call MoveRightLine_Y3

        ret

MACRO MergeLeftLine
        ; Line 1
        ; Merge 1 into 0
        ld d, CELL_X1_Y\1
        ld e, CELL_X0_Y\1
        call MergeTwoCells

        ; Merge 2 into 1, if 1 not merged into 0
        and a
        jr nz, .merge_line\1_3_into_2
        ld d, CELL_X2_Y\1
        ld e, CELL_X1_Y\1
        call MergeTwoCells

        ; Merge 3 into 2
.merge_line\1_3_into_2:
        ld d, CELL_X3_Y\1
        ld e, CELL_X2_Y\1
        call MergeTwoCells
        ENDM

MACRO MoveLeftLine_one_pass_m
MoveLeftLine_one_pass_Y\1:
        ld d, CELL_X1_Y\1
        ld e, CELL_X0_Y\1
        call MoveCell
        ld d, CELL_X2_Y\1
        ld e, CELL_X1_Y\1
        call MoveCell
        ld d, CELL_X3_Y\1
        ld e, CELL_X2_Y\1
        call MoveCell

        ret
        ENDM

MACRO MoveLeftLine
        MoveLeftLine_one_pass_m \1
MoveLeftLine_Y\1:
        ; 3 passes
        call MoveLeftLine_one_pass_Y\1
        call MoveLeftLine_one_pass_Y\1
        call MoveLeftLine_one_pass_Y\1
        ret
        ENDM

        MoveLeftLine 0
        MoveLeftLine 1
        MoveLeftLine 2
        MoveLeftLine 3

PushLeft::
        xor a
        ld [wAnEventOccurred], a

        ; A first push
        call MoveLeftLine_Y0
        call MoveLeftLine_Y1
        call MoveLeftLine_Y2
        call MoveLeftLine_Y3

        ; The merge of cells
        MergeLeftLine 0
        MergeLeftLine 1
        MergeLeftLine 2
        MergeLeftLine 3

        ; A second push, because the merge create empty cells.
        call MoveLeftLine_Y0
        call MoveLeftLine_Y1
        call MoveLeftLine_Y2
        call MoveLeftLine_Y3

        ret

; Merge two cells into one.
; @input d : cell merged from.
; @input e : cell merged into.
; @Return if merge occured in a
MergeTwoCells:
        ; load d cell value
        ld hl, wCurrentCellPhasesTable
        ld b, 0
        ld c, d
        add hl, bc
        ld a, [hl]

        ; Check if d cell not equal zero, if equal : not merge
        and a
        jp z, .not_merged

        ; b now contain d cell value
        ld b, a
        push bc

        ; load e cell value
        ld hl, wCurrentCellPhasesTable
        ld b, 0
        ld c, e
        add hl, bc
        ld a, [hl]

        ; compare e cell and d cell, merge if equal, if not equal : not merge
        pop bc
        cp b
        jp nz, .not_merged

        ; increment cell e, set 0 to cell d (merge)
        inc a
        ld [hl], a

        xor a
        ld hl, wCurrentCellPhasesTable
        ld b, 0
        ld c, d
        add hl, bc
        ld [hl], a

        ; A move occured
        ld a, [wAnEventOccurred]
        set 1, a
        ld [wAnEventOccurred], a

        ; return true
        ld a, 1
        ret

.not_merged:
        ; return false
        xor a
        ret

; Move cell into another if the receptor is empty (phase 0).
; @input d : emitter 
; @input e : receptor
MoveCell:
        ; load emitter cell value
        ld hl, wCurrentCellPhasesTable
        ld b, 0
        ld c, d
        add hl, bc
        ld a, [hl]

        ; Check if emitter cell is not equal zero, if zero : not moved
        and a
        ret z

        ; Save the emitter cell value
        ld b, a
        push bc

        ; load receptor cell value
        ld hl, wCurrentCellPhasesTable
        ld b, 0
        ld c, e
        add hl, bc
        ld a, [hl]

        ; Check if receptor cell is equal zero, if not zero : not moved
        pop bc
        and a
        ret nz

        ; Get the emittor cell value back.
        ld a, b

        ; Receptor cell value become the emitter cell value
        ld [hl], a

        ; Set the emitter value to zero
        xor a
        ld hl, wCurrentCellPhasesTable
        ld b, 0
        ld c, d
        add hl, bc
        ld [hl], a

        ; A move occured
        ld a, [wAnEventOccurred]
        set 0, a
        ld [wAnEventOccurred], a
        ret

SECTION "GridMovements_Variables", WRAM0

;bit 0 : a move occurred
;bit 1 : a merge occurred
wAnEventOccurred:: ds 1