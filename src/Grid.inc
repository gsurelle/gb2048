; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

DEF GRID_HEIGHT             equ 4
DEF GRID_WIDTH              equ 4
DEF CELLS_COUNT             equ GRID_WIDTH * GRID_HEIGHT

DEF CELL_X0_Y0              equ $00
DEF CELL_X1_Y0              equ $01
DEF CELL_X2_Y0              equ $02
DEF CELL_X3_Y0              equ $03
DEF CELL_X0_Y1              equ $04
DEF CELL_X1_Y1              equ $05
DEF CELL_X2_Y1              equ $06
DEF CELL_X3_Y1              equ $07
DEF CELL_X0_Y2              equ $08
DEF CELL_X1_Y2              equ $09
DEF CELL_X2_Y2              equ $0a
DEF CELL_X3_Y2              equ $0b
DEF CELL_X0_Y3              equ $0c
DEF CELL_X1_Y3              equ $0d
DEF CELL_X2_Y3              equ $0e
DEF CELL_X3_Y3              equ $0f