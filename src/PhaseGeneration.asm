; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

include "src/Grid.inc"
include "hardware.inc"

SECTION "PhaseGeneration", ROM0

FillCellsEmptyArray:
        ; Clear the array.
        xor a
        ld [wEmptyCellsCounter], a

        ; Lets fill the array with empty cells index.
        ld hl, wCurrentCellPhasesTable
        ld b, CELLS_COUNT
        ld c, $00

; for each cell in wCurrentCellPhasesTable
.loop:

        ; Is cell value empty ?
        ldi a, [hl]
        and a
        jp nz, .not_empty
.empty
        push hl

        ; Push back the empty cell idx, in wEmptyCellsTable
        ld hl, wEmptyCellsTable 
        xor a
        ld d, a
        ld a, [wEmptyCellsCounter]
        ld e, a
        add hl, de ; hl contains wEmptyCellsTable back address.

        ld a, c
        ld [hl], a

        ; wEmptyCellsCounter++
        ld a, [wEmptyCellsCounter]
        inc a
        ld [wEmptyCellsCounter], a

        pop hl

.not_empty:

.end:
        inc c
        dec b
        jp nz, .loop
        ret

FindNewPhaseReceptorCellIdx:
        ; Pointing hl to the empty cells counter random table.
        ; Example: there is 4 cells left, we pointing to the
        ; random table that go from $00 to $03.
        
        ld hl, RandomTableNewPhaseReceptorCellIdx

        xor a
        ld d, a
        ld a, [wEmptyCellsCounter]
        dec a
        swap a
        ld e, a
        ; => de now contains the offset.

        add hl, de
        ; => hl now contains the random table address, first byte.


        ; //////////////////////
        ; adding a random offset to hl, only 4 lsb.
        xor a
        ld d, a
        ld a, [rDIV]
        and a, $0f
        ld e, a

        add hl, de
        ; => hl now contains a random receptor cell id address.
        
        ; //////////////////////
        ; get the cell id that's going to be the receptor cell.
        xor a
        ld d, a
        ld a, [hl]
        ld e, a
        ld hl, wEmptyCellsTable
        add hl, de
        ld a, [hl]
        ld [wNewPhaseReceptorCellIdx], a

        ret

SetupNewPhaseReceptorCellValue:
        ; Generate a random number between 1 and 2,
        ; and set the new phase receptor cell.

        ld hl, RandomTableNewPhaseValue

        xor a
        ld d, a
        ld a, [rDIV]
        and a, $0f
        ld e, a
        ; => de contans a random offset between 0 and f.

        add hl, de
        ; => hl now contains a random new phase value address.
        ld a, [hl]
        ld b, a
        ; => b now contains the new phase random value.

        ; ////////////////
        ; Load the phase value into the receptor cell.
        ld hl, wCurrentCellPhasesTable

        xor a
        ld d, a
        ld a, [wNewPhaseReceptorCellIdx]
        ld e, a
        add hl, de

        ld a, b
        ld [hl], a

        ret

CreateNewPhase::
        call FillCellsEmptyArray
        call FindNewPhaseReceptorCellIdx
        call SetupNewPhaseReceptorCellValue
        ret

SECTION "PhaseGeneration_Variables", WRAM0
; cells that can accept a new phase
wEmptyCellsCounter:       ds 1
wEmptyCellsTable:         ds CELLS_COUNT
wNewPhaseReceptorCellIdx: ds 1