; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

include "hardware.inc"
include "src/Grid.inc"
include "memory.inc"

DEF FIRST_CELL_X0_Y0        equ $9822
DEF CELL_SIZE               equ 3
DEF CELL_BORDER_SIZE        equ 1

SECTION "GameRender", ROM0

; a = times we have to add bc to hl
AddOffset:
        inc a
        jr .skip

.loop:
        add hl, bc

.skip:
        dec a
        jr nz, .loop

        ret

add_b_to_de:
        ld a, e
        adc b 
        ld e, a
        jr nc, .endif
        inc d
.endif
        ret

DrawCell:
        push hl
        push de

        ; reset bc
        ld bc, 0

        ; -------------------------
        ; hl have to contain the destination
        ld hl, _SCRN0 + GRID_START_MARGIN - CELL_ROW_OFFSET
        ;
        ; adding the tilemap offset
        ; -> y first
        ld a, [wCurrentCellDrew_Idx]
        and a, %00001100
        sra a
        sra a
        ld c, $80 ; $80 is the y offset per row.
        call AddOffset

        ; -> x second
        ld a, [wCurrentCellDrew_Idx]
        and a, %00000011
        ld c, $04 ; $04 is the x offset per column.
        call AddOffset
        ; => Now de contains the starting address tile map for the cell.
        

        ; -------------------------
        ; wCurrentCellDrew_Tile have to contain the first
        ; tile map id for the current cell phase value.
        ld a, [wCurrentCellDrew_Phase]
        inc a
        swap a
        ld [wCurrentCellDrew_Tile], a

DEF CELL_ROW_SIZE   EQU $04
DEF CELL_ROW_OFFSET EQU $20 - CELL_ROW_SIZE
DEF GRID_START_MARGIN EQU $22

        ld b, $4 ; number of rows to write 
        inc b
        jr .skip_1
.loop_1
        ld de, CELL_ROW_OFFSET ;
        add hl, de             ; Apply the row offset.

        ld c, $4 ; number of tile to write
        inc c
        jr .skip_2
.loop_2:

        lcd_WaitVRAM

        ld a, [wCurrentCellDrew_Tile] ;
        ld [hli], a                   ; Write tile map id

        inc a                         ;
        ld [wCurrentCellDrew_Tile], a ; Increment wCurrentCellDrew_Tile

.skip_2
        dec c 
        jr nz, .loop_2

.skip_1:
        dec b
        jr nz, .loop_1 
        
        pop de
        pop hl
        ret


        ;------------------------------------
        ;------------------------------------
        ;------------------------------------
        ;------------------------------------
        ;------------------------------------
DrawGrid::
        ; loop on current phases table.
        ld de, $10
        inc e
        jr .skip
.loop
        ld hl, wCurrentCellPhasesTable
        add hl, de
        dec hl

        ; save cell phase value
        ld a, [hl]
        ld [wCurrentCellDrew_Phase], a

        ; save cell phase idx
        ld a, e
        dec a
        ld [wCurrentCellDrew_Idx], a
        
        call DrawCell

.skip
        dec e
        jr nz, .loop
        ret


SECTION "GridRenderer_variables", WRAM0

wCurrentCellDrew_Idx:     ds 1
wCurrentCellDrew_Phase:   ds 1
wCurrentCellDrew_Tile:    ds 1