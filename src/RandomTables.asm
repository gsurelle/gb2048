; MIT License
; 
; Copyright (c) 2021 Geoffrey SURELLE
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

SECTION "RandomTableNewPhaseReceptorCellIdx", ROM0[$3000]
RandomTableNewPhaseReceptorCellIdx::
	;$00
	DB $00,$00,$00,$00,$00,$00,$00,$00
	DB $00,$00,$00,$00,$00,$00,$00,$00

	;$01
	DB $00,$01,$00,$01,$00,$01,$00,$01
	DB $00,$01,$00,$01,$00,$01,$00,$01

	;$02
	DB $00,$01,$02,$00,$01,$02,$00,$01
	DB $02,$00,$01,$02,$00,$01,$02,$00

	;$03
	DB $00,$01,$02,$03,$00,$01,$02,$03
	DB $00,$01,$02,$03,$00,$01,$02,$03

	;$04
	DB $00,$01,$02,$03,$04,$00,$01,$02
	DB $03,$04,$00,$01,$02,$03,$04,$00

	;$05
	DB $00,$01,$02,$03,$04,$05,$00,$01
	DB $02,$03,$04,$05,$00,$01,$03,$04

	;$06
	DB $00,$01,$02,$03,$04,$05,$06,$00
	DB $01,$02,$03,$04,$05,$06,$00,$01

	;$07
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $00,$01,$02,$03,$04,$05,$06,$07

	;$08
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $08,$00,$01,$02,$03,$04,$05,$06

	;$09
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $08,$09,$00,$01,$02,$03,$04,$05

	;$0a
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $08,$09,$0a,$00,$01,$02,$03,$04

	;$0b
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $08,$09,$0a,$0b,$00,$01,$02,$03

	;$0c
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $08,$09,$0a,$0b,$0c,$00,$01,$02

	;$0d
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $08,$09,$0a,$0b,$0c,$0d,$00,$01

	;$0e
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $08,$09,$0a,$0b,$0c,$0d,$0e,$00

	;$0f
	DB $00,$01,$02,$03,$04,$05,$06,$07
	DB $08,$09,$0a,$0b,$0c,$0d,$0e,$0f

RandomTableNewPhaseValue::
	; Normally its 10% of 4, but here we set 12,5%
	DB $01,$01,$01,$01,$01,$01,$01,$02
	DB $01,$01,$01,$01,$01,$01,$01,$01